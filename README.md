# README #

### What is this repository for? ###

* CSCE 1040
* Lab 8
* Problem 3

### How do I get set up? ###

* Compile the program with gcc -o run-prog letterpairs.c
* Run the program with ./run-prog
* Input is read from the 'wordpairs' file
* Output is printed to the terminal